from setuptools import setup


setup(name='fixer-demo',
      version='0.3',
      description='Fixer service demo package',
      url='https://gitlab.com/meshkat_mohammadi/fixer-demo',
      author='Meshkat',
      author_email='mahdieh.mohammadi1997@gmail.com',
      license='MIT',
      packages=['fixer'],
      install_requires=['requests'],
      zip_safe=False)
